#####
# 2 step analysis on big data
#####


################################################################################
# load library
################################################################################
import os
import pandas as pd
import numpy as np
from time import time
from sklearn.cluster import Birch, MiniBatchKMeans, KMeans
from sklearn.preprocessing import scale
import matplotlib
matplotlib.use('Agg')  # plotting backend compatible with screen
import sys
import scanpy as sc


################################################################################
# set pars
################################################################################
input_file = sys.argv[1]
output_final_sub = "Scanpy" + "_" + sys.argv[2]
res = float(sys.argv[3])
nnb = int(sys.argv[4])
npcs = sys.argv[5]
if npcs != "None":
	npcs = int(npcs)


################################################################################
# perform clustering and tSNE
################################################################################
if os.path.isfile(output_final_sub + ".h5ad"):
	print("Final clustering result exists... Skip")
	adata = sc.read_h5ad(output_final_sub + ".h5ad")
else:
	## settings
	# sc.settings.verbosity = 2  # show logging output
	sc.settings.autosave = True  # save figures, do not show them
	sc.settings.set_figure_params(dpi=400)  # set sufficiently high resolution for saving

	## perform clustering and tSNE
	adata = sc.read_text(input_file, first_column_names=True)
	sc.pp.neighbors(adata, n_neighbors=nnb)
	sc.tl.louvain(adata, resolution=res)
	adata.write(output_final_sub + ".h5ad")
	adata.write_csvs(output_final_sub + ".csv")







